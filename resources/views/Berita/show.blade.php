@extends('master')

@section('navbar2')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="page-header-title">
                    <h5 class="m-b-10">Table Berita</h5>
                    <p class="m-b-0">Berisi Semua Berita</p>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html"> <i class="fa fa-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="/berita">Berita</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="card">
    <div class="card-body m-3">
        <h4> {{$berita->judul}} </h4>
        <p> {{$berita->content}} </p>
        <p>{{$berita->penulis}}</p>
    </div>
</div>
@endsection