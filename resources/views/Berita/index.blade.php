@extends('master')

@section('navbar2')
<div class="page-header">
    <div class="page-block">
        <div class="row align-items-center">
            <div class="col-md-8">
                <div class="page-header-title">
                    <h5 class="m-b-10">Table Berita</h5>
                    <p class="m-b-0">Berisi Semua Berita</p>
                </div>
            </div>
            <div class="col-md-4">
                <ul class="breadcrumb-title">
                    <li class="breadcrumb-item">
                        <a href="index.html"> <i class="fa fa-home"></i> </a>
                    </li>
                    <li class="breadcrumb-item"><a href="/berita">Berita</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div>
@endsection

@section('content')
<div class="m-3">
    <div class="card">
        <!-- /.card-header -->
        <div class="card-body">
            @if (session('success'))
                <div class="alert alert-success">
                    {{ session('success') }}
                </div>
            @endif
            <a class="btn btn-primary mb-3" href=" {{ route('berita.create')}} ">Buat Berita Baru</a>
          <table class="table table-bordered">
            <thead>
              <tr>
                <th style="width: 10px">No</th>
                <th>Judul</th>
                <th>Konten</th>
                <th>Penulis</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($berita as $key => $news)
                  <tr>
                      <td> {{ $key + 1 }} </td>
                      <td> {{ $news->judul }} </td>
                      <td> {{ $news->konten }} </td>
                      <td> {{ $news->penulis }} </td>
                      <td style="display: flex">
                        {{-- <a href="{{route('berita.show', ['berita'=>$news->id])}}" class="btn btn-info btn-sm">Info</a>
                        <a href="{{route('berita.edit', ['berita'=>$news->id])}}" class="btn btn-default btn-sm">Edit</a> --}}
                        <a href="{{route('berita.show', $news->id)}}" class="btn btn-info btn-sm">Info</a>
                        <a href="{{route('berita.edit', $news->id)}}" class="btn btn-default btn-sm">Edit</a>
                        <form action="/berita/{{$news->id}}" method="post">
                            @csrf
                            @method('DELETE')
                        <input type="submit" value="delete" class="btn btn-danger btn-sm"></form>
                    </td>
                  </tr>
                  @empty
                      <tr>
                          <td colspan="5" align="center">No Post</td>
                      </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.card-body -->
        {{-- <div class="card-footer clearfix">
          <ul class="pagination pagination-sm m-0 float-right">
            <li class="page-item"><a class="page-link" href="#">«</a></li>
            <li class="page-item"><a class="page-link" href="#">1</a></li>
            <li class="page-item"><a class="page-link" href="#">2</a></li>
            <li class="page-item"><a class="page-link" href="#">3</a></li>
            <li class="page-item"><a class="page-link" href="#">»</a></li>
          </ul>
        </div> --}}
    </div>
</div>
@endsection